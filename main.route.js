(function () {

    function rssFeedRoute($stateProvider, $urlRouterProvider) {
        
        $urlRouterProvider.otherwise("/index");
        
        $stateProvider
            .state('index', {
                url: "",
                views: {
                    "mainContent": {
                        templateUrl: "MainContent/main.tmpl.html",
                         controller: 'mainCtrl as main'
                    },
                    "sideBar": {
                        templateUrl: "SideBar/sidebar.tmpl.html",
                         controller: 'sidebarCtrl as side'
                    }
                }
            })
            .state('route1', {
                url: "/route1",
                views: {
                    "viewA": {
                        template: "route1.viewA"
                    },
                    "viewB": {
                        template: "route1.viewB"
                    }
                }
            })
            .state('route2', {
                url: "/route2",
                views: {
                    "viewA": {
                        template: "route2.viewA"
                    },
                    "viewB": {
                        template: "route2.viewB"
                    }
                }
            })
    }
    
    angular.module('rssFeed').config(rssFeedRoute);

})();