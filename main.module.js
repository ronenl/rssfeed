(function () {
    angular.module('rssFeed', ['ui.router', 'rssFeedServices', 'rssFeedDirectives']);


    angular.module('rssFeed').filter('reverse', function () {
        return function (items) {
            return items.slice().reverse();
        };
    });
})();