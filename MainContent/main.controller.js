(function () {

    function mainCtrl(DataService, $scope) {
        var vm = this;

        vm.items = [];
        vm.title = '';

        $scope.$on('URL_ENTER', function (e, obj) {
            DataService.getFeeds(obj.url).then(function (res) {
                vm.items = [];
                vm.items = res.data.responseData.feed.entries;
                vm.title = obj.url;
            });
        });
    }


    angular.module('rssFeed').controller('mainCtrl', ['DataService', '$scope', mainCtrl]);
})();