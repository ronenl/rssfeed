(function () {

    function sidebarCtrl(DataService, $rootScope, $scope) {

        var vm = this;

        
        vm.selectedUrl = {};
        
        vm.urls = DataService.getItems();

        vm.addUrl = function () {
            var newObj = {'key': vm.urls.length,'value': vm.inputUrl};
            vm.urls.push(newObj);
            vm.selectedUrl = newObj;
            vm.inputUrl = '';
            DataService.saveItem(vm.urls);
        }

        vm.selectUrl = function (url) {
            vm.selectedUrl = url;
        }

        
        vm.removeUrl = function(key){
            _.remove(vm.urls,function(url){
                return url.key == key;
            });
            
            DataService.saveItem(vm.urls);
        }
        

        $scope.$watch(angular.bind(this, function () {
            return this.selectedUrl; // `this` IS the `this` above!!
        }), function (newVal, oldVal) {
            if ( newVal != oldVal)
                $rootScope.$broadcast('URL_ENTER', {
                    url: newVal.value
                });
        });

    }


    angular.module('rssFeed').controller('sidebarCtrl', ['DataService', '$rootScope', '$scope', sidebarCtrl]);
})();