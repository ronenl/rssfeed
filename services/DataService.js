(function () {

    var services = angular.module('rssFeedServices', []);

    function DataService($http, encodeUri) {

        var self = this;

        self.getFeeds = function (url) {
            return $http({
                method: 'JSONP',
                url: 'http://ajax.googleapis.com/ajax/services/feed/load?callback=JSON_CALLBACK&v=1.0&num=50&q=' + url

            });
        }
        
        self.saveItem = function(list){
           // var list = angular.fromJson(localStorage.getItem('rssList')) || [];
            
            
            
            localStorage.setItem('rssList', angular.toJson(list));
        }
        
        self.getItems = function(){
            var list = angular.fromJson(localStorage.getItem('rssList')) || [];
            
            return list;
        }
    }

    services.service('DataService', ['$http', DataService]);

})();