(function () {

    var directives = angular.module('rssFeedDirectives', ['ngSanitize']);

    function feedItem() {
        return {
            restrict: 'EA',
            scope: {
                data: '='
            },
            templateUrl: '/directives/feedItem.tmpl.html',
            controller: function ($scope) {
                $scope.formatDate = function (date) {
                    var dateOut = new Date(date);
                    return dateOut;
                };
            }
        }
    }

    directives.directive('feedItem', [feedItem]);
})();